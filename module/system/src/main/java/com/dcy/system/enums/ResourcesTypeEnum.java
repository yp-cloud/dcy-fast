package com.dcy.system.enums;

import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description: 资源类型枚举类
 * @Date: 2021/8/24 11:04
 */
public enum ResourcesTypeEnum {

    //资源 类型（0、模块；1、链接）
    MODULE("0", "模块"),
    LINK("1", "链接"),
    ;

    public final String code;
    public final String name;

    ResourcesTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ResourcesTypeEnum getByCode(String code) {
        return Stream.of(ResourcesTypeEnum.values())
                .filter(resourcesTypeEnum -> resourcesTypeEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }
}
