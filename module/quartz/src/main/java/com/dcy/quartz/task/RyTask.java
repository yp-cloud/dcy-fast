package com.dcy.quartz.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Slf4j
@Component("ryTask")
public class RyTask {

    /**
     * 调度任务有参方法（多参数）
     *
     * @param s
     * @param b
     * @param l
     * @param d
     * @param i
     */
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i) {
        log.info("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i);
    }

    /**
     * 调度任务有参方法
     *
     * @param params
     */
    public void ryParams(String params) {
        log.info("执行有参方法 {}", params);
    }

    /**
     * 调度任务无参方法
     */
    public void ryNoParams() {
        log.info("执行无参方法");
    }
}
