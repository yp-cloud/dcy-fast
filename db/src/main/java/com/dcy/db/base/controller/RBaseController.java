package com.dcy.db.base.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.dcy.common.context.BaseContextHandler;
import com.dcy.common.model.R;
import com.dcy.common.model.ReturnCode;
import com.dcy.db.base.model.PageResult;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/20 16:30
 */
public class RBaseController {

    /**
     * 转换page
     *
     * @param iPage model分页数据
     * @param list  dto数据
     * @return
     */
    protected <DTO, T> PageResult<DTO> toPageDTO(IPage<T> iPage, List<DTO> list) {
        return PageResult.toPageDTO(iPage, list);
    }

    /**
     * 获取用户id
     */
    protected String getUserId() {
        return BaseContextHandler.getUserID();
    }

    /**
     * 获取用户名称
     */
    protected String getUsername() {
        return BaseContextHandler.getUsername();
    }


    protected <T> R<T> success() {
        return R.success();
    }

    protected <T> R<T> success(ReturnCode returnCode, T obj) {
        return R.success(returnCode, obj);
    }

    protected <T> R<T> success(T obj) {
        return R.success(obj);
    }

    protected <T> R<T> error() {
        return R.error();
    }

    protected <T> R<T> error(ReturnCode returnCode, T obj) {
        return R.error(returnCode, obj);
    }

    protected <T> R<T> error(Integer code, String msg) {
        R<T> r = new R<>();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }

    protected <T> R<T> error(ReturnCode returnCode) {
        return R.error(returnCode);
    }

}
