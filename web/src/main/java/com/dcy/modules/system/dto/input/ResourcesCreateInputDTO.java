package com.dcy.modules.system.dto.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:01
 */
@Getter
@Setter
@ApiModel(value="ResourcesCreateInputDTO", description="创建资源使用")
public class ResourcesCreateInputDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    @ApiModelProperty(value = "父级ids")
    private String parentIds;

    @ApiModelProperty(value = "资源名称")
    private String resName;

    @ApiModelProperty(value = "资源code")
    private String resCode;

    @ApiModelProperty(value = "资源path")
    private String resPath;

    @ApiModelProperty(value = "请求方式")
    private String httpMethod;

    @ApiModelProperty(value = "状态（0、正常；1、禁用）")
    private String resStatus;

    @ApiModelProperty(value = "类型（0、模块；1、链接）")
    private String resType;

    @ApiModelProperty(value = "排序")
    private BigDecimal resSort;
}
