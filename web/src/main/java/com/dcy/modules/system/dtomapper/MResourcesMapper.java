package com.dcy.modules.system.dtomapper;

import com.dcy.modules.system.dto.input.ResourcesCreateInputDTO;
import com.dcy.modules.system.dto.input.ResourcesUpdateInputDTO;
import com.dcy.modules.system.dto.output.ResourcesListOutputDTO;
import com.dcy.system.model.Resources;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper(componentModel = "spring")
public interface MResourcesMapper {

    /**
     * 创建DTO 转换 resources
     *
     * @param resourcesCreateInputDTO
     * @return
     */
    Resources resourcesCreateInputDTOToResources(ResourcesCreateInputDTO resourcesCreateInputDTO);

    /**
     * 修改DTO 转换 resources
     *
     * @param resourcesUpdateInputDTO
     * @return
     */
    Resources resourcesUpdateInputDTOToResources(ResourcesUpdateInputDTO resourcesUpdateInputDTO);

    ResourcesListOutputDTO resourcesToResourcesListOutputDTO(Resources resources);

    List<ResourcesListOutputDTO> resourcesToResourcesListOutputDTO(List<Resources> resources);
}
